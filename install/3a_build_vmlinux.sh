#!/bin/bash

sudo apt-get install -y build-essential libncurses-dev bison flex libssl-dev libelf-dev

# ##########################################################
# LINUXVM
############################################################

cd build_vm/custom_vmlinux

git clone https://github.com/torvalds/linux.git linux.git
cd linux.git
git checkout v4.20
cp ../.config  ./

make menuconfig

# --------- MANUAL STEPS: IN GUI ---------
# -> Simply exit GUI, using default config
# ----------------------------------------

make vmlinux

mv vmlinux ../
cd ..
rm -rf linux.git