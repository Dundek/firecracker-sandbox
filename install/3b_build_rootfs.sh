#!/bin/bash

# ##########################################################
# ROOTFS
############################################################cd ..

IMG_NAME="lambda_node"

cd build_vm/custom_rootfs

rm -rf ./$IMG_NAME.ext4
rm -rf ./vm_init


docker build -t $IMG_NAME .

dd if=/dev/zero of=$IMG_NAME.ext4 bs=1M count=100
mkfs.ext4 $IMG_NAME.ext4
mkdir -p mnt
sudo mount $IMG_NAME.ext4 mnt

# --------- VM Init script example ---------
# cat > ./vm_nginx << EOF
# #!/bin/sh

# mkdir -p /var/log/nginx

# # the init process should never exit
# /usr/local/sbin/nginx -g "daemon off;"
# EOF
# ----------------------------------

# --------- VM Init script ---------
cat > ./vm_init << EOF
#!/bin/sh

#/usr/bin/node /index.js
echo "Hello firecracker"
EOF
# ----------------------------------

docker run --rm -ti -v $(pwd)/mnt:/my-rootfs $IMG_NAME

sudo mv mnt/sbin/init mnt/sbin/init.old
sudo cp vm_init mnt/sbin/init
sudo chmod a+x mnt/sbin/init

sudo umount mnt

rm -rf ./mnt
rm -rf ./vm_init