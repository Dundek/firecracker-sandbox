#!/bin/bash

# Install KVM
sudo apt install -y qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager

# Configure networking
IFACE_NAME="ens33"

cat > /etc/network/interfaces << EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo br0
iface lo inet loopback

iface $IFACE_NAME inet manual

iface br0 inet dhcp
    bridge_ports $IFACE_NAME
EOF

# Add user to binaries
sudo adduser $USER libvirt
sudo adduser $USER libvirt-qemu

# Reboot
sudo reboot