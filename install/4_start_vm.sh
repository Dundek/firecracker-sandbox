#!/bin/bash

VMLINUX_BIN=$(pwd)/build_vm/custom_vmlinux/vmlinux
ROOTFS_EXT4=$(pwd)/build_vm/custom_rootfs/lambda_node.ext4

cat > ./vm_config.json << EOF
{
  "boot-source": {
    "kernel_image_path": "$VMLINUX_BIN",
    "boot_args": "console=ttyS0 reboot=k panic=1 pci=off"
  },
  "drives": [
    {
      "drive_id": "rootfs",
      "path_on_host": "$ROOTFS_EXT4",
      "is_root_device": true,
      "is_read_only": false
    }
  ],
  "machine-config": {
    "vcpu_count": 2,
    "mem_size_mib": 1024,
    "ht_enabled": false
  }
}
EOF

rm -rf /tmp/firecracker.socket
firecracker --api-sock /tmp/firecracker.socket --config-file ./vm_config.json