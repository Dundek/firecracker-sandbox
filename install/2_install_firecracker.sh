#!/bin/bash

# Update APT
sudo apt-get -y update

# Install Docker
sudo apt-get -y install docker.io
sudo systemctl start docker
sudo usermod -aG docker $USER
sudo systemctl enable docker

# Grant read/write access to kvm
sudo setfacl -m u:${USER}:rw /dev/kvm

# Down load latest firecracker binary
latest=$(basename $(curl -fsSLI -o /dev/null -w  %{url_effective} https://github.com/firecracker-microvm/firecracker/releases/latest))
curl -LOJ https://github.com/firecracker-microvm/firecracker/releases/download/${latest}/firecracker-${latest}-$(uname -m)

# Rename the binary
mv firecracker-${latest}-$(uname -m) firecracker

# Make it executable
chmod +x firecracker

# Move the binary to /usr/bin
sudo mv firecracker /usr/bin